from flask_godmode.actions.ban import BanAction
from flask_godmode.base.delete_view import BaseDeleteView
from flask_godmode.base.details_view import BaseDetailsView
from flask_godmode.base.list_view import BaseListView
from flask_godmode.base.model import BaseAdminModel
from flask_godmode.base.widget import BaseWidget
from flask_godmode.common.acl import ACL
from flask_godmode.groups.demo import DemoGroup
from flask_godmode.widgets.boolean import BooleanReverseWidget

from db.demo import User, DemoDatabase


class NameWidget(BaseWidget):
    filterable = False

    def render_list(self, item):
        return "<b>{}</b>".format(item.name)


class UsersAdminModel(BaseAdminModel):
    db = DemoDatabase
    name = "users"
    title = "Users"
    icon = "icon-user"
    group = DemoGroup
    index = 100
    table = User
    widgets = {
        "is_locked": BooleanReverseWidget
    }

    class PUsersListView(BaseListView):
        title = "User list"
        sorting = ["id", "name"]
        default_sorting = User.created_at.desc()
        fields = [
            "id",
            "name",
            "created_at",
            "post_count",
            "is_locked"
        ]
        object_actions = [BanAction]
        batch_actions = [BanAction]
        widgets = {
            "name": NameWidget
        }

    list_view = PUsersListView
